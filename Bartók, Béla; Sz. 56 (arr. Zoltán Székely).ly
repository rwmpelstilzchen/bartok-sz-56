\version "2.18.2"

\header {
  title = "Román népi táncok"
  subtitle = "Sz. 56, BB 68"
  composer = "Béla Bartók"
  arranger = \markup{\right-column{"Aranĝado por violino kaj piano" "per Zoltán Székely"}}
}

handupbow = \upbow
handdownbow = \downbow

\header {
  piece = "Bot tánc / Jocul cu bâtă (danco del' bastono)"
}

violinA = {
  \clef treble
  \time 2/4
  \tempo "Allegro moderato" 4=80
  <<
    {
      r2 r2 r2 r4
    }\\
    {
      \small
      a, < c e > |
      a, < c e > |
      a, < c e > |
      a,
    }
  >>
  r8\f c-2\tenuto\upbow |
  \barNumberCheck #5
  b,16\tenuto c\tenuto d4\4\tenuto c8\tenuto                    |
  d16-1\4(\< e\4) e4\4( fis8-02\4)\!                            |
  fis16-02\4( g-03\4) g4\4( f16\4 e\4)                          |
  e16-2\4( d\4) c8\staccato c\staccato r8                       |
  b,16-01\tenuto\downbow c\tenuto d4\4~ d16 b,32\handupbow( d\4 |
  \barNumberCheck #10
  c8.)\< a,16( b,8)\! g,\staccato\handdownbow                                                             |
  a,2\handdownbow\sf                                                                                      |
  \acciaccatura gis,8( a,4..\handupbow) c'!16\upbow                                                       |
  \once \set fingeringOrientations = #'(down) <b\2 fis\3 d-04\4>16\tenuto c'\tenuto d'4\tenuto c'8\tenuto |
  \once \set fingeringOrientations = #'(down) <d' e a,-01>16( e') <e' e'>4( fis'8)                        |
  \barNumberCheck #15
  <fis' d' d g,>16( g') g'4( f'16 e')                                                          |
  <e'\2 e\3 g,\4>16( d') c'8\staccato <c' e c>\staccato r                                      |
  <b e>16\tenuto\downbow\< c'\tenuto d'8\staccato d'\staccato\handupbow( b\staccato\handupbow) |
  \acciaccatura d'8-02( <c'-01 a-03>8) <a\3 c> \acciaccatura c'8-02( <b-01 d>) <g b,>\!        |
  <a\3 cis>2\sf                                                                                |

  %tablature ^

  \barNumberCheck #20
  \acciaccatura gis,8-01( a,4..-01\sf\handupbow) g?16-01\upbow\mf |
  a4.-02 \tuplet 3/2 {c'16-02\handupbow( b c'}                    |
  d'8. a16) d'8\staccato b\staccato                               |
  c'4. b8(                                                        |
  c'8) r16 e'-0( a'8-1\staccato) b'\staccato\handupbow            |
  \barNumberCheck #25
  c''8\portato r16 b'\handupbow( a'8) g'-04      |
  f'8\portato r16 e'-02\(( d'8) b-01\)           | % unclear hand
  d'16-03( c') c'4\handupbow( b8)                |
  c'8.\(( b16) c'8\handupbow\)( g\handupbow)     | % hand slurs
  a4.-02\> \tuplet 3/2 {c'16-02\handupbow( b c'} |
  \barNumberCheck #30
  d'8. a16)\! d'8\staccato b\staccato                                                  |
  d'16( c') c'4( \tuplet 3/2 {d'16 c' d'16)}                                           |
  e'8.( c'16) g8\staccato r                                                            |
  <a-02 a-00>8^"pizz."_\markup{\italic{cresc. molto}} e'\stopped^"G" <a a>8 e'\stopped | % ‘G’ in hand
  <a a>8 e'\stopped <a a>8 e'\stopped                                                  |
  \barNumberCheck #35
  <a a>2\sf^\markup{\center-column{"T" "T"}}                   | % ‘T’ in hand; ‘ar00’?
  \acciaccatura gis,8-01( a,4..-01\sf\handupbow) g?16-01\upbow | % ‘3’?
  <a-00 e c>4. \tuplet 3/2 {c'16( b-01 c'}                     |
  d'8. a16) d'8\staccato b\staccato                            |
  <d' f a,>16( c') c'4( b8                                     |
  \barNumberCheck #40
  c'8) r16 e'( a'8-01\staccato) b'\staccato                                        |
  <c'' e' g>8\portato r16 b'\(( a'8) g'\)                                          | % ?
  <f' c' d>8\portato r16 e'-04\(( d'8) b\staccato\)                                | % fingering; ?
  <d' f g,>16( c') c'4( b8)                                                        |
  \acciaccatura d'8( <c' g'>8.)\((\> b16) c'8\handupbow\)( g\staccato\handupbow)\! | % hand slurs
  \barNumberCheck #45
  \once \set fingeringOrientations = #'(down) <a dis-01>4. \tuplet 3/2 {c'16( b c'}          |
  d'8. a16) d'8\staccato b\staccato                                                          |
  <d' g a,>16( c') c'4( \tuplet 3/2 {d'16 c' d')}                                            |
  e'8.( c'16) g8\staccato r                                                                  |
  \acciaccatura gis8-01 <a-02 a>8\staccato_\markup{\italic{cresc. molto}} e' a'\staccato e'' | % ‘0’ harmonics?
  \barNumberCheck #50
  \acciaccatura gis'8-01( a'8-02) e'' \harmonicsOn d'8-03\staccato_"I" a'8-03_\markup{\italic{poco allarg.}} \harmonicsOff |
  \acciaccatura gis'8-02( a'2-03)\sf                                                                                       |
  \acciaccatura gis,8( a,2)\sf \bar "|."
}

<<
  %\override Score.BarNumber #'break-visibility = #'#(#f #t #t)
  %\set Score.barNumberVisibility = #(every-nth-bar-number-visible 5)
  \new Staff \with {\omit StringNumber} {
    \transpose c, c {\violinA}
  }
  \new TabStaff {
    \set TabStaff.stringTunings = #mandolin-tuning
    \transpose c, c {\violinA}
  }
>>

\score {
  \new Staff \with {midiInstrument = #"violin"} {
	\unfoldRepeats
	\transpose c c' {\violinA}
  }
  \midi { }
}
